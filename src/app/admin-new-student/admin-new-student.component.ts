import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { InitService } from '../init/init.service';
import Swal from 'sweetalert2';
import { Student } from '../add-student/add-student.component';
@Component({
  selector: 'app-admin-new-student',
  templateUrl: './admin-new-student.component.html',
  styleUrls: ['./admin-new-student.component.scss']
})
export class AdminNewStudentComponent implements OnInit {
  allNewStudents:any[] = [];
  searchText:string= '';
  constructor(private toastr: ToastrService, private spinner: NgxSpinnerService, private _initService: InitService) { }

  ngOnInit(): void {
    this.getAllNewStudents();
  }
  getAllNewStudents() {
    this.spinner.show();
    this._initService.getAllNewStudents().subscribe(data => {
      this.allNewStudents = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  changeActiveStatus(event, item) {
    let status = event.target.checked ? 'Confirm' : 'Pending';
    if (event.target.checked === true) {
      // Handle your code
    }
    Swal.fire({
      title: 'Are you sure?',
      text: "Do you want to update the Status to " + status + "?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, update it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.updateActiveStatus(event.target.checked, item);
      } else {
        this.allNewStudents = JSON.parse(JSON.stringify(this.allNewStudents));
      }
    });
  }
  updateActiveStatus(status: boolean, item) {
    let body:any = {};
    body.is_confirmed = status? 'Y':'N';
    this._initService.updateNewStudent(item._id, body).subscribe(data => {
      if (data && data.status) {
        if(status){
          this.addStudent(item);
        }else{
          this.removeStudent(item.email);
        }
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      this.allNewStudents = JSON.parse(JSON.stringify(this.allNewStudents));
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  addStudent(item:any) {
    this.spinner.show();
    let body = new Student();
    body.email = item.email;
    body.name = item.name;
    body.nic_number = 'Not Set';
    body.conact_number = item.contact_number;
    body.gender = 'not_set';
    this._initService.studentCreate(body).subscribe(data => {
      if (data && data.status) {
        this.getAllNewStudents();
        Swal.fire(
          'Updated!',
          'Status has been updated.',
          'success'
        );
        this.allNewStudents = JSON.parse(JSON.stringify(this.allNewStudents));
      } else {
        this.spinner.hide();
        this.getAllNewStudents();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        });
        this.allNewStudents = JSON.parse(JSON.stringify(this.allNewStudents));
      }
    }, err => {
      this.getAllNewStudents();
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
      this.allNewStudents = JSON.parse(JSON.stringify(this.allNewStudents));
    });

  }
  removeStudent(email) {
    this.spinner.show();
    this._initService.getStudentByEmail(email).subscribe(realData=>{
        if(realData.data){
          this._initService.removeStudent(realData.data._id).subscribe(data => {
            this.spinner.hide();
            Swal.fire(
              'Updated!',
              'Status has been updated.',
              'success'
            );
            this.getAllNewStudents();
            this.allNewStudents = JSON.parse(JSON.stringify(this.allNewStudents));
          }, error => {
            this.spinner.hide();
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Something went wrong!'
            });
            this.getAllNewStudents();
            this.allNewStudents = JSON.parse(JSON.stringify(this.allNewStudents));
          });
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!'
          });
          this.getAllNewStudents();
        }
    },(error)=>{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
      this.getAllNewStudents();
    });
   
  }
}
