import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNewStudentComponent } from './admin-new-student.component';

describe('AdminNewStudentComponent', () => {
  let component: AdminNewStudentComponent;
  let fixture: ComponentFixture<AdminNewStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNewStudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNewStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
