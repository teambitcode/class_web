import { Component, OnInit } from '@angular/core';
import { Routes } from '@angular/router';
import { AboutUsComponent } from '../about-us/about-us.component';
import { ClassCardsComponent } from '../class-cards/class-cards.component';
import { ContentComponent } from '../content/content.component';
import { NewStudentsComponent } from '../new-students/new-students.component';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

export const aboutChildRoutes : Routes = [
 
  {
    path: '',
    component: AboutUsComponent
  }, {
    path: 'about-us',
    component: AboutUsComponent
  },
  {
    path: 'classes',
    component: ClassCardsComponent
  },
  {
    path: 'content',
    component: ContentComponent
  },
  {
    path: 'register-me',
    component: NewStudentsComponent
  }
];
