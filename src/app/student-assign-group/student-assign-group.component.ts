import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-student-assign-group',
  templateUrl: './student-assign-group.component.html',
  styleUrls: ['./student-assign-group.component.scss']
})
export class StudentAssignGroupComponent implements OnInit {
  public studentAddGroupForm: FormGroup;
  selectedClassId: string = '';
  isValidCladd = false;
  isValidGroup = false;
  selectedClassName :string = '';
  selectedClassGropuName :string = '';
  currentStudentId= '';
  currentGroupId = '';
  constructor(private formBuilder: FormBuilder) { }
  @Input() allClass: any[] = [];
  @Input() allClassGroups: any[] = [];
  @Input() groupsDetails: any[] = [];
  @Input() currentStudent: any = null;
  @Output() addEditEvent = new EventEmitter();

  ngOnInit(): void {
    this.studentAddGroupForm = this.formBuilder.group({
      'classId': [null, Validators.compose([
        Validators.required
      ])],
      'groupId': [null, Validators.compose([
        Validators.required
      ])]
    });
  }
  removeConfirm(id){
    this.currentGroupId = id;
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
       let data =  this.removetudentInGroup(id);
       this.addRemoveEventCall('EDIT',data,id)
      }
    });
  }

  removetudentInGroup(id) {
    let studentFilter: any[] = [];
    let slectedGroup: any[] = this.allClassGroups.filter(classGroupFilterItem => {
      return classGroupFilterItem._id === id;
    });
    if (slectedGroup && slectedGroup.length > 0) {
      studentFilter = slectedGroup[0].students.filter(filterStudentItem => {
        return filterStudentItem.detail._id !== this.currentStudent._id;
      });
    }
    return studentFilter;
  }
  onSelectClassId(event) {
    this.selectedClassId = event.target.value;
    if (event.target.value && event.target.value !== "null") {
      this.selectedClassName = this.getNameFromId(this.allClass,event.target.value);
      this.isValidCladd = true;
    } else {
      this.isValidCladd = false;
    }
  }
  getNameFromId(array:any[],id){
   let returnValueArray = array.filter(filterItem=>{
      return filterItem._id === id;
    });
    if(returnValueArray && returnValueArray.length>0){
      return returnValueArray[0].name;
    }else{
      return '';
    }
  }
  onSelectGroupId(event) {
    if (event.target.value && event.target.value !== "null") {
      this.selectedClassGropuName = this.getNameFromId(this.allClassGroups,event.target.value);
      this.isValidGroup = true;
    } else {
      this.isValidGroup = false;
    }
  }
  findStudentInGroup() {
    let studentFilter: any[] = [];
    let slectedGroup: any[] = this.allClassGroups.filter(classGroupFilterItem => {
      return classGroupFilterItem._id === this.studentAddGroupForm.get('groupId').value;
    });
    if (slectedGroup && slectedGroup.length > 0) {
      studentFilter = slectedGroup[0].students.filter(filterStudentItem => {
        return filterStudentItem.detail._id === this.currentStudent._id;
      });
    }
    return studentFilter.length > 0;
  }
  addRemoveEventCall(event,students,id) {
  
    let eventData = new EventOut();
    eventData.data = {};
    eventData.data.data = students;
    eventData.data.id = id;
    if (event === 'ADD') {
      eventData.event = 'ADD';
    } else {
      eventData.event = 'EDIT';
    }
    this.addEditEvent.emit(eventData);
  }
  addEditEventCall(event) {
    let group_id = this.studentAddGroupForm.get('groupId').value;
    let eventData = new EventOut();
    eventData.data = group_id;
    if (event === 'ADD') {
      eventData.event = 'ADD';
    } else {
      eventData.event = 'EDIT';
    }
    this.addEditEvent.emit(eventData);
  }
  addStudentToGroupConfirm(event) {
    if (!this.findStudentInGroup()) {
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#21d44a',
        cancelButtonColor: 'rgb(251 29 29)',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
          this.addEditEventCall(event);
        }
      });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Student already in this group!'
      });
    }


  }
}
export class EventOut {
  event: string;
  data: any;
}