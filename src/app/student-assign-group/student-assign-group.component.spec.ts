import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentAssignGroupComponent } from './student-assign-group.component';

describe('StudentAssignGroupComponent', () => {
  let component: StudentAssignGroupComponent;
  let fixture: ComponentFixture<StudentAssignGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentAssignGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentAssignGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
