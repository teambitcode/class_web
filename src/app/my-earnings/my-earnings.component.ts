import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { JwtService } from '../infrastructure/jwt.service';
import { InitService } from '../init/init.service';

@Component({
  selector: 'app-my-earnings',
  templateUrl: './my-earnings.component.html',
  styleUrls: ['./my-earnings.component.scss']
})
export class MyEarningsComponent implements OnInit {
  allClassGroups: any[] = [];
  userId: any = '';
  constructor(private _jwtService: JwtService, private spinner: NgxSpinnerService, private _initService: InitService) {
    this.getAllClassGroups();
  }
  getUser() {
    this.spinner.show();
    this.userId = this._jwtService.getId();
    this._initService.getUser(this.userId).subscribe(data => {
      if (data && data.status) {

      }
    });
  }
  ngOnInit(): void {
    this.getAllClassGroups();
    this.getUser();
  }
  getAllClassGroups() {
    this.spinner.show();
    this._initService.getAllGroups().subscribe(data => {
      this.allClassGroups = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  getTotalAmountForAllGroups() {
    let amount = 0;
    this.allClassGroups.forEach((element) => {
      amount += this.getTotalAmount(element);
    });
    return parseFloat(amount.toString()).toFixed(2);
  }
  getTotalAmount(grpDataElement) {
    let totalAmount = 0;
    if (grpDataElement.mentor._id === this.userId) {
      grpDataElement.students.forEach(element => {
        totalAmount += element.payment_amount;
      });
    }
    return totalAmount;
  }
  getPaymentStatus() {

  }
}
