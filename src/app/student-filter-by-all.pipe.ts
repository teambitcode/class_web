import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterStudentByAll'
})
export class filterStudentByAllPipe implements PipeTransform {
  transform(data:any[],value: string) {
    if(data && data.length>0){
      return data.filter((item:any)=> {
          return (item.email.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1) ||
                 (item.name.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1) ||
                 (item.nic_number.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1) ||
                 (item.conact_number.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1)
      });
    }else{
        return [];
    }
  
}
}