import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { InitService } from '../init/init.service';

@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.scss']
})
export class ClassesComponent implements OnInit {
  allClass:any[] = [];
  constructor(private spinner: NgxSpinnerService, private _initService: InitService) { 
    this.getAllClass();
  }

  ngOnInit(): void {
    this.getAllClass();
  }
  editClass(item){

  }
  removeConfirm(id){
    
  }
  getAllClass() {
    this.spinner.show();
    this._initService.getAllClasses().subscribe(data => {
      this.allClass = data.data;
      this.spinner.hide();
    },error=>{
      this.spinner.hide();
    });
  }
}
