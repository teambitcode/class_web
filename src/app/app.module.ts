import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { AppFooterComponent } from './app-footer/app-footer.component';
import { AppLandingComponent } from './app-landing/app-landing.component';
import { AppStudentComponent } from './app-student/app-student.component';
import { AppRegisterComponent } from './app-register/app-register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { filterPipe } from './filter.pipe';
import { filterMentorPipe } from './filter-mentor.pipe';
import { SideBarComponent } from './side-bar/side-bar.component';
import { AppLoginComponent } from './app-login/app-login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppEditUserComponent } from './app-edit-user/app-edit-user.component';
import { HomeComponent } from './home/home.component';
import { ClassesComponent } from './classes/classes.component';
import { MyEarningsComponent } from './my-earnings/my-earnings.component';
import { ProfileComponent } from './profile/profile.component';
import { SendEmailsComponent } from './send-emails/send-emails.component';
import { AdminAreaComponent } from './admin-area/admin-area.component';
import { AddMentorComponent } from './add-mentor/add-mentor.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { AddClassComponent } from './add-class/add-class.component';
import { AddClassGroupComponent } from './add-class-group/add-class-group.component';
import { MainService } from './infrastructure/api.service';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from "ngx-spinner";
import { UserModalComponent } from './user-modal/user-modal.component';
import { ClassGroupsComponent } from './class-groups/class-groups.component';
import { StudentAssignGroupComponent } from './student-assign-group/student-assign-group.component';
import { GroupDetailModalComponent } from './group-detail-modal/group-detail-modal.component';
import { StudentPaymentComponent } from './student-payment/student-payment.component';
import {NewStudentsComponent} from './new-students/new-students.component';
import { AboutComponent } from './about/about.component';
import { ClassCardsComponent } from './class-cards/class-cards.component';
import { ContentComponent } from './content/content.component';
import { AboutNavComponent } from './about-nav/about-nav.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { ExpandableListModule } from 'angular-expandable-list';
import { AdminNewStudentComponent } from './admin-new-student/admin-new-student.component';
import { FilterClassGroupByDetails } from './class-group-filter.pipe';
import { filterStudentByAllPipe } from './student-filter-by-all.pipe';
import { filterNewStudentByAllPipe } from './new-student-filter.pipe';
import { FilterPaymentEmailPipe } from './student-payment-email.pipe';
import { GraphsComponent } from './graphs/graphs.component';
import { ChartsModule } from 'ng2-charts';
import { CountInArrayPipe } from './count-in-array.pipe';
@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    AppFooterComponent,
    AppLandingComponent,
    AppStudentComponent,
    AppRegisterComponent,
    filterPipe,
    filterNewStudentByAllPipe,
    FilterClassGroupByDetails,
    FilterPaymentEmailPipe,
    filterMentorPipe,
    filterStudentByAllPipe,
    SideBarComponent,
    AppLoginComponent,
    AppEditUserComponent,
    HomeComponent,
    ClassesComponent,
    MyEarningsComponent,
    ProfileComponent,
    SendEmailsComponent,
    AdminAreaComponent,
    AddMentorComponent,
    AddStudentComponent,
    AddClassComponent,
    AddClassGroupComponent,
    UserModalComponent,
    ClassGroupsComponent,
    StudentAssignGroupComponent,
    GroupDetailModalComponent,
    StudentPaymentComponent,
    NewStudentsComponent,
    AboutComponent,
    ClassCardsComponent,
    ContentComponent,
    AboutNavComponent,
    AboutUsComponent,
    AdminNewStudentComponent,
    GraphsComponent,
    CountInArrayPipe
  ],
  imports: [
    ModalModule.forRoot(),
    ToastrModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgbModule,
    ExpandableListModule,
    ChartsModule
  ],
  providers: [MainService],
  bootstrap: [AppComponent]
})
export class AppModule { }
