import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { InitService } from '../init/init.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-student-payment',
  templateUrl: './student-payment.component.html',
  styleUrls: ['./student-payment.component.scss']
})
export class StudentPaymentComponent implements OnInit {
  allClassGroups:any[] = [];
  allStudents:any[] = [];
  currentPayment:any = null;
  currentPaymentStatus:any = null;
  public studentPaymentForm: FormGroup;
  selectedGroup:any= null;
  selectedStudent:any= null;
  constructor(private formBuilder: FormBuilder,private spinner: NgxSpinnerService, private _initService: InitService) { 
    this.getAllStudents();
    this.getAllClassGroups();
  }
  getAllClassGroups() {
    this.spinner.show();
    this._initService.getAllGroups().subscribe(data => {
      this.allClassGroups = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  getAllStudents() {
    this.spinner.show();
    this._initService.getAllStudents().subscribe(data => {
      this.allStudents = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  ngOnInit(): void {
    this.studentPaymentForm = this.formBuilder.group({
      'inputGroupId': [null, Validators.compose([
        Validators.required
      ])],
      'inputStudentId': [null, Validators.compose([
        Validators.required
      ])]
      ,
      'inputAmount': [null, Validators.compose([
        Validators.required
      ])]
      ,
      'isFullyPaid': [null, Validators.compose([])]
    });
  }
  onSelectStudentId(event){
    let tempSelectedStudent:any[] = [];
    console.log(event.target.value);
    tempSelectedStudent =  this.allStudents.filter(filterItem=>{
      return filterItem._id === event.target.value;
    });
    if(tempSelectedStudent.length>0){
      this.selectedStudent = tempSelectedStudent[0];
      this.getCurrentPaidAmountForStudentInGroupsArray();
    }else{
      this.selectedStudent = null;
    }
  }
  onSelectGroupId(event){
    let tempSelectedGroup:any[] = [];
    console.log(event.target.value);
   tempSelectedGroup =  this.allClassGroups.filter(filterItem=>{
      return filterItem._id === event.target.value;
    });
    if(tempSelectedGroup.length>0){
      this.selectedGroup = tempSelectedGroup[0];
    }else{
      this.selectedGroup = null;
    }
  }
  getCurrentPaidAmountForStudentInGroupsArray(){
    let selectedGroupItem:any[] = [];
    let selectedStudentItem:any[] = [];
    selectedGroupItem =  this.allClassGroups.filter(itemFilter=>{
      return itemFilter._id===this.selectedGroup._id;
    });
    if(selectedGroupItem && selectedGroupItem.length>0){
      selectedStudentItem =   selectedGroupItem[0].students.filter(itemFilterStudent=>{
        return itemFilterStudent.detail._id===this.selectedStudent._id;
      });
      if(selectedStudentItem && selectedStudentItem.length>0){
        this.currentPayment= selectedStudentItem[0].payment_amount;
        this.currentPaymentStatus = selectedStudentItem[0].payment_status;
      }
    }
  }
  updateGroupCall(){
    let studentUpdated = this.updateSlectedGroupStudentArray();
    if(studentUpdated && studentUpdated.length>0 && this.selectedGroup._id)
    this._initService.updateGroups(this.selectedGroup._id, {students:studentUpdated}).subscribe(data => {
      this.allClassGroups = data.data;
      this.spinner.hide();
      Swal.fire(
        'Updated!',
        'Payment has been updated.',
        'success'
      );
      this.getAllStudents();
      this.getAllClassGroups();
    }, error => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  onCnfirmPayment(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();
        this.updateGroupCall();
      }
    });
  }
  updateSlectedGroupStudentArray() {
    let studentArray:any[]=[];
    let student:any = {};
    let paidAmount  = this.studentPaymentForm.get('inputAmount').value;
    let paidStatus  = this.studentPaymentForm.get('isFullyPaid').value;
    let selectedStudentItem:any[] = [];
    let selectedGroup = this.allClassGroups.filter(filterGroupItem => {
      return filterGroupItem._id === this.selectedGroup._id;
    });
    if (selectedGroup && selectedGroup.length > 0) {
      selectedStudentItem =  selectedGroup[0].students.filter(itemFilterStudent=>{
        return itemFilterStudent.detail._id===this.selectedStudent._id;
      });
      if(selectedStudentItem && selectedStudentItem.length>0){
        selectedStudentItem[0].payment_amount = paidAmount;
        selectedStudentItem[0].payment_status = paidStatus? "PAID": "PENDING";
        studentArray = selectedGroup[0].students;
      
      }
    } else {
      return null;
    }
    return studentArray;
  }

  
}
