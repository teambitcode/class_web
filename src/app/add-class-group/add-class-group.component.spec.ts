import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddClassGroupComponent } from './add-class-group.component';

describe('AddClassGroupComponent', () => {
  let component: AddClassGroupComponent;
  let fixture: ComponentFixture<AddClassGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddClassGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddClassGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
