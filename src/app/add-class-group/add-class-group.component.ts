import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { InitService } from '../init/init.service';
import Swal from 'sweetalert2'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-add-class-group',
  templateUrl: './add-class-group.component.html',
  styleUrls: ['./add-class-group.component.scss']
})
export class AddClassGroupComponent implements OnInit {
  allClass: any[] = [];
  allClassGroups: any[] = [];
  allMentors: any[] = [];
  allStudents: any[] = [];
  public classGroupForm: FormGroup;
  currentClassGroup: any = null;
  currentClassGroupId = null;
  isEditMode = false;
  modalRef: BsModalRef;
  totalAmount: number = 0;
  isValidClassGroupStatus = false;
  selectedClassGroupStatus = null;
  searchText: string = '';
  constructor(private modalService: BsModalService, private spinner: NgxSpinnerService, private _initService: InitService,
    private formBuilder: FormBuilder) {
    this.getAllClass();
    this.getAllClassGroups();
    this.getAllMentors();
    this.getAllStudents();
  }

  ngOnInit(): void {
    this.classGroupForm = this.formBuilder.group({
      'groupId': [null, Validators.compose([
        Validators.required
      ])],
      'groupName': [null, Validators.compose([
        Validators.required
      ])],
      'classId': [null, Validators.compose([
        Validators.required
      ])],
      'mentor': [null, Validators.compose([
        Validators.required
      ])],
      'price': [null, Validators.compose([
        Validators.required
      ])],
      'status': [null, Validators.compose([])]
      ,
      'mpStatus': [null, Validators.compose([])]
    });
  }
  addClassGroup() {
    this.spinner.show();
    let body = new Classes();
    body.class_id = this.classGroupForm.get('classId').value;
    body.group_id = this.classGroupForm.get('groupId').value;
    body.name = this.classGroupForm.get('groupName').value;
    body.price = this.classGroupForm.get('price').value;
    body.mentor = this.classGroupForm.get('mentor').value;
    this._initService.groupsCreate(body).subscribe(data => {
      if (data && data.status) {
        this.getAllClassGroups();
        this.resetForm();
        this.spinner.hide();
        Swal.fire(
          'Added!',
          'Class group has been added.',
          'success'
        );
      } else {
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        });
      }
    }, err => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });

  }
  getAllClass() {
    this.spinner.show();
    this._initService.getAllClasses().subscribe(data => {
      this.allClass = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  getAllStudents() {
    this.spinner.show();
    this._initService.getAllStudents().subscribe(data => {
      this.allStudents = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  getAllMentors() {
    this.spinner.show();
    this._initService.getAllUsers().subscribe(data => {
      this.allMentors = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  getAllClassGroups() {
    this.spinner.show();
    this._initService.getAllGroups().subscribe(data => {
      this.allClassGroups = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  removeClassGroupStudentFromGroupInDeleteState(studentId,groupId) {
    this.spinner.show();
    this._initService.removeStudentInGroupInDeleteState(studentId,groupId).subscribe(data => {
      this.spinner.hide();
      this.modalRef.hide();
      this.getAllClass();
      this.getAllClassGroups();
      this.getAllMentors();
      this.getAllStudents();
      Swal.fire(
        'Deleted!',
        'Student removed from class group',
        'success'
      );
    }, error => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  removeClassGroupStudentFromGroup(studentId) {
    this.spinner.show();
    this._initService.removeStudentInGroup(studentId, this.currentClassGroupId).subscribe(data => {
      this.spinner.hide();
      this.modalRef.hide();
      this.getAllClass();
      this.getAllClassGroups();
      this.getAllMentors();
      this.getAllStudents();
      Swal.fire(
        'Deleted!',
        'Student removed from class group',
        'success'
      );
    }, error => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  removeClassGroup() {
    this.spinner.show();
    this._initService.removeGroups(this.currentClassGroupId).subscribe(data => {
      this.spinner.hide();
      this.getAllClassGroups()
      Swal.fire(
        'Deleted!',
        'Class Group has been deleted.',
        'success'
      );
    }, error => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  removeConfirm(id) {
    this.currentClassGroupId = id;
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.removeClassGroup();
      }
    });
  }
  removeStudentInGroupConfirm(item) {
    let studentId = null;
    if (item && item.detail && item.detail._id) {
      studentId = item.detail._id;
    } else {
      studentId = null;
    }
    if (this.currentClassGroupId && studentId) {
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#21d44a',
        cancelButtonColor: 'rgb(251 29 29)',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.removeClassGroupStudentFromGroup(studentId);
        }
      });
    } else {

      if (item._id) {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#21d44a',
          cancelButtonColor: 'rgb(251 29 29)',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.removeClassGroupStudentFromGroupInDeleteState(item._id,this.currentClassGroupId);
          }
        });
      } else {
        Swal.fire(
          {
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!'
          }
        );
      }

    }

  }
  updateChagesConfirm() {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, update it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.saveChanges();
      }
    });
  }
  onSelectClassGroupStatus(event) {
    this.selectedClassGroupStatus = event.target.value;
    if (event.target.value && event.target.value !== "null") {
      this.isValidClassGroupStatus = true;
    } else {
      this.isValidClassGroupStatus = false;
    }
  }
  saveChanges() {
    this.spinner.show();
    let body = new Classes();
    body.class_id = this.classGroupForm.get('classId').value;
    body.name = this.classGroupForm.get('groupName').value;
    body.mentor = this.classGroupForm.get('mentor').value;
    body.price = this.classGroupForm.get('price').value;
    body.group_status = this.classGroupForm.get('status').value;
    body.mentor_payment_status = this.classGroupForm.get('mpStatus').value;
    this._initService.updateGroups(this.currentClassGroupId, body).subscribe(data => {
      if (data && data.status) {
        this.resetForm();
        this.getAllClassGroups();
        Swal.fire(
          'Updated!',
          'Class Group has been updated.',
          'success'
        );
        this.isEditMode = false;
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  displayMode(template: TemplateRef<any>, item) {
    this.currentClassGroup = item;
    this.currentClassGroupId = item._id;
    this.getTotalAmount();
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }
  getTotalAmount() {
    let amount = 0;
    this.totalAmount;
    this.currentClassGroup.students.forEach(element => {
      amount += element.payment_amount;
    });
    this.totalAmount = JSON.parse(JSON.stringify(amount));
  }
  editClass(item) {
    this.isEditMode = true;
    this.currentClassGroupId = item._id;
    this.currentClassGroup = item;
    this.setFormFromGivenClassGroup(item);
  }
  resetForm() {
    this.classGroupForm.reset();
  }
  cancel() {
    this.resetForm();
    this.isEditMode = false;
    this.currentClassGroupId = null;
    this.currentClassGroup = null;
  }
  setFormFromGivenClassGroup(classGroupDetails) {
    this.classGroupForm.patchValue({ classId: classGroupDetails.class_id._id });
    this.classGroupForm.patchValue({ groupId: classGroupDetails.group_id });
    this.classGroupForm.patchValue({ groupName: classGroupDetails.name });
    this.classGroupForm.patchValue({ mentor: classGroupDetails.mentor._id });
    this.classGroupForm.patchValue({ price: classGroupDetails.price });
    this.classGroupForm.patchValue({ status: classGroupDetails.group_status });
    this.classGroupForm.patchValue({ mpStatus: classGroupDetails.mentor_payment_status });
    this.isValidClassGroupStatus = true;

  }

}

export class Classes {
  class_id: string;
  group_id: string;
  name: string;
  mentor: string;
  price: number;
  group_status: string;
  mentor_payment_status: string;
}

