import { Injectable } from '@angular/core';

// import common service

import { Observable } from 'rxjs';
import { MainService } from '../infrastructure/api.service';

@Injectable({
  providedIn: 'root',
})
export class InitService {
  constructor(private _apiService: MainService) { }
  loggedUser: string = window.sessionStorage.getItem('userId');
  userCreate(obj: any): Observable<any> {
    return this._apiService.post(`user/new`, obj);
  }
  userLogin(obj: any): Observable<any> {
    return this._apiService.post(`user/login`, obj);
  }
  getAllUsers(): Observable<any> {
    return this._apiService.get(`user/getAll`);
  }
  updateUser(id: any, obj: any): Observable<any> {
    return this._apiService.patch(`user/update/${id}`, obj);
  }
  updateUserPassword(id: any, obj: any): Observable<any> {
    return this._apiService.post(`user/update/password/${id}`, obj);
  }
  getUser(id: any): Observable<any> {
    return this._apiService.get(`user/get/` + id);
  }
  removeUser(id: any): Observable<any> {
    return this._apiService.delete(`user/delete/` + id);
  }


  studentCreate(obj: any): Observable<any> {
    return this._apiService.post(`student/new`, obj);
  }
  studentPaymentEmail(obj: any): Observable<any> {
    return this._apiService.post(`student/payment-email`, obj);
  }
  getAllStudents(): Observable<any> {
    return this._apiService.get(`student/getAll`);
  }
  getAllStudentPaymentDetails(): Observable<any> {
    return this._apiService.get(`student/getAll`);
  }
  updateStudents(id: any, obj: any): Observable<any> {
    return this._apiService.patch(`student/update/${id}`, obj);
  }
  getStudents(id: any): Observable<any> {
    return this._apiService.get(`student/get/` + id);
  }
  getStudentByEmail(email: string): Observable<any> {
    return this._apiService.get(`student/get-by-email/` + email);
  }
  getAllStudentPayments(): Observable<any> {
    return this._apiService.get(`student/payment-email`);
  }
  removeStudent(id: any): Observable<any> {
    return this._apiService.delete(`student/remove/` + id);
  }
  removeNewStudent(id: any): Observable<any> {
    return this._apiService.delete(`new-student/remove/` + id);
  }


  classesCreate(obj: any): Observable<any> {
    return this._apiService.post(`classes/new`, obj);
  }
  newStudentCreate(obj: any): Observable<any> {
    return this._apiService.post(`new-student/new`, obj);
  }

  getAllClasses(): Observable<any> {
    return this._apiService.get(`classes/getAll`);
  }
  getAllNewStudents(): Observable<any> {
    return this._apiService.get(`new-student/getAll`);
  }
  updateClasses(id: any, obj: any): Observable<any> {
    return this._apiService.patch(`classes/update/${id}`, obj);
  }
  updateNewStudent(id: any, obj: any): Observable<any> {
    return this._apiService.patch(`new-student/update/${id}`, obj);
  }
  getClasses(id: any): Observable<any> {
    return this._apiService.get(`classes/get/` + id);
  }
  removeClasses(id: any): Observable<any> {
    return this._apiService.delete(`classes/delete/` + id);
  }


  groupsCreate(obj: any): Observable<any> {
    return this._apiService.post(`groups/new`, obj);
  }
  getAllGroups(): Observable<any> {
    return this._apiService.get(`groups/getAll`);
  }
  updateGroups(id: any, obj: any): Observable<any> {
    return this._apiService.patch(`groups/update/${id}`, obj);
  }
  getGroups(id: any): Observable<any> {
    return this._apiService.get(`groups/get/` + id);
  }
  removeGroups(id: any): Observable<any> {
    return this._apiService.delete(`groups/delete/` + id);
  }
  removeStudentInGroup(idStudent: string,idGroup:string): Observable<any> {
    return this._apiService.delete(`groups/remove-student/?student_id=`+idStudent+`&group_id=`+idGroup);
  }
  removeStudentInGroupInDeleteState(idStudent: string,idGroup:string): Observable<any> {
    return this._apiService.delete(`groups/remove-student/delete-state/?student_id=`+idStudent+`&group_id=`+idGroup);
  }
}
