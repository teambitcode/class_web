import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterGroupsByMentor'
})
export class filterMentorPipe implements PipeTransform {
  transform(data:any[],value: string) {
    return data.filter(item=> item.mentor._id=== value)
  }
}