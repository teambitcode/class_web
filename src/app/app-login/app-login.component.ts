import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JwtService } from '../infrastructure/jwt.service';
import { InitService } from '../init/init.service';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2';
@Component({
  selector: 'app-app-login',
  templateUrl: './app-login.component.html',
  styleUrls: ['./app-login.component.scss']
})
export class AppLoginComponent implements OnInit {
  public onLoginForm: FormGroup;
  constructor(private spinner: NgxSpinnerService,private router: Router,private _initService: InitService,
    private formBuilder: FormBuilder, private _jwtService:JwtService) {
   }

  ngOnInit(): void {
    this.onLoginForm = this.formBuilder.group({
      'email': [null, Validators.compose([
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required
      ])]
    });
  }
  login(){
    this.spinner.show();
    let body = new Login();
    body.email = this.onLoginForm.get('email').value;
    body.password = this.onLoginForm.get('password').value;
    this._initService.userLogin(body).subscribe(data=>{
      if(data && data.status){
        this._jwtService.saveServiceToken(data.token);
        this._jwtService.saveToken(data.token);
        this._jwtService.saveId(data.data.id);
        this.router.navigateByUrl('/base/home');
      }else{
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: data.msg?data.msg:'Something went wrong!'
        });
      }
    },err=>{
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: err.msg?err.msg:'Something went wrong!'
      });
    });

  }
}

export class Login{
  email: string;
  password: string;
}
