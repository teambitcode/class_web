import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { InitService } from '../init/init.service';
import { DataService } from '../data.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-send-emails',
  templateUrl: './send-emails.component.html',
  styleUrls: ['./send-emails.component.scss']
})
export class SendEmailsComponent implements OnInit {
  public emailForm: FormGroup;
  allClassGroups: any[] = [];
  allPaymentDetails: any[] = [];
  allStudents: any[] = [];
  currentId = null;
  currentClassName = null;
  searchText = '';
  public model: any;
  isValidStudent = false;
  constructor(private spinner: NgxSpinnerService, private _initService: InitService,
    private formBuilder: FormBuilder, private dataService: DataService) { }

  ngOnInit(): void {
    if(this.dataService.student && this.dataService.student._id){
      this.isValidStudent = true;
    }else{
      this.isValidStudent = false;
    }
    this.getAllClassGroups();
    this.getAllPaymentEmails();
    this.getAllStudents();
    this.emailForm = this.formBuilder.group({
      'total': [null, Validators.compose([
        Validators.required
      ])],
      'description': [null, Validators.compose([
        Validators.required
      ])],
      'class_id': [null, Validators.compose([
        Validators.required
      ])],
      'subject': [null, Validators.compose([
        Validators.required
      ])]
    });
  }
  getAllClassGroups() {
    this.spinner.show();
    this._initService.getAllGroups().subscribe(data => {
      this.allClassGroups = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  getAllPaymentEmails() {
    this.spinner.show();
    this._initService.getAllStudentPayments().subscribe(data => {
      this.allPaymentDetails = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  getAllStudents() {
    this.spinner.show();
    this._initService.getAllStudents().subscribe(data => {
      this.allStudents = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  sendEmailConfirm() {
    if (this.dataService && this.dataService.student && this.dataService.student._id) {
      this.currentClassName = this.emailForm.get('class_id').value;
      let body = new Email();
      body.id = this.dataService.student._id;
      body.date = this.dataService.formatDate(new Date());
      body.class = this.emailForm.get('class_id').value.class_id.name;
      body.class_id = this.emailForm.get('class_id').value._id;
      body.payment_type = 'Bank Deposit';
      body.description = this.emailForm.get('description').value;
      body.total = this.parseFloatWithTwoDecimal(this.emailForm.get('total').value.toString());
      body.subject = this.emailForm.get('subject').value;
      let html = `
      <h2>Please Check Below details are correct</h2>
      <ul style="text-align: left !important;">
      <li>Student ID : `+ this.dataService.student._id + `</li>
      <li>Student Name : `+ this.dataService.student.name + `</li>
      <li>Student Email : `+ this.dataService.student.email + `</li>
      <li>Class : `+ this.emailForm.get('class_id').value.class_id.name + `</li>
      <li>Payment Type : Bank Deposit</li>
      <li>Description : `+ this.emailForm.get('description').value + `</li>
      <li>Total : `+ this.parseFloatWithTwoDecimal(this.emailForm.get('total').value.toString()) + `</li>
      <li>Subject : `+ this.emailForm.get('subject').value + `</li>
    </ul>  `
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        html: html,
        showCancelButton: true,
        confirmButtonColor: '#21d44a',
        cancelButtonColor: 'rgb(251 29 29)',
        confirmButtonText: 'Yes, Send it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.sendEmail();
        }
      });
    } else {
      Swal.fire(
        'Ooops!',
        'Student id not found!',
        'success'
      );
    }
  }
  parseFloatWithTwoDecimal(value: string) {
    return parseFloat(value).toFixed(2);
  }
  sendEmail() {
    if (this.dataService && this.dataService.student && this.dataService.student._id) {
      this.spinner.show();
      this.currentClassName = this.emailForm.get('class_id').value;
      let body = new Email();
      body.id = this.dataService.student._id;
      body.date = this.dataService.formatDate(new Date());
      body.class = this.emailForm.get('class_id').value.class_id.name;
      body.class_id = this.emailForm.get('class_id').value._id;
      body.payment_type = 'Bank Deposit';
      body.description = this.emailForm.get('description').value;
      body.total = this.parseFloatWithTwoDecimal(this.emailForm.get('total').value.toString());
      body.subject = this.emailForm.get('subject').value;
      this._initService.studentPaymentEmail(body).subscribe(data => {
        if (data && data.status) {
          this.getAllStudents();
          Swal.fire(
            'Success!',
            'Email has been sent.',
            'success'
          );
          this.resetForm();
        } else {
          this.spinner.hide();
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!'
          });
        }
      }, err => {
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        });
      });

    } else {
      Swal.fire(
        'Ooops!',
        'Student id not found!',
        'success'
      );
    }

  }
  resetForm() {
    this.emailForm.reset();
  }

  setMessage(message){
    this.emailForm.patchValue({ description: message } );
  }

  resetMessage(){
    this.emailForm.patchValue({ description: "" } );
  }
}

export class Email {
  id: string;
  date: string;
  class: string;
  class_id: string;
  subject: string;
  payment_type: string;
  description: string;
  total: string;
}