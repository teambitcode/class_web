import { Component, OnInit } from '@angular/core';
import { Routes } from '@angular/router';
import { AdminAreaComponent } from '../admin-area/admin-area.component';
import { AppRegisterComponent } from '../app-register/app-register.component';
import { AppStudentComponent } from '../app-student/app-student.component';
import { ClassGroupsComponent } from '../class-groups/class-groups.component';
import { ClassesComponent } from '../classes/classes.component';
import { GraphsComponent } from '../graphs/graphs.component';
import { HomeComponent } from '../home/home.component';
import { MyEarningsComponent } from '../my-earnings/my-earnings.component';
import { ProfileComponent } from '../profile/profile.component';
import { SendEmailsComponent } from '../send-emails/send-emails.component';

@Component({
  selector: 'app-landing',
  templateUrl: './app-landing.component.html',
  styleUrls: ['./app-landing.component.scss']
})
export class AppLandingComponent implements OnInit {
  value:number = 0;
  isAdmin = false;
  constructor() { }
students :any[] = [
  {"name":"John", "age":"23", "city":"Agra"},
  {"name":"Steve", "age":"28", "city":"Delhi"},
  {"name":"Peter", "age":"32", "city":"Chennai"},
  {"name":"Chaitanya", "age":"28", "city":"Bangalore"}
];
  ngOnInit(): void {
   let idValue =  sessionStorage.getItem('id');
   this.isAdmin = true;
  }
  getStudents():any[]{
    return this.students.filter(item=> item.age >= this.value)
  }

}



export const landingChildRoutes : Routes = [
  {
    path: 'classes',
    component: ClassesComponent
  },
  {
    path: 'class-group',
    component: ClassGroupsComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'students',
    component: AppStudentComponent
  }
  ,
  {
    path: 'my-earnings',
    component: MyEarningsComponent
  },
  {
    path: 'graphs',
    component: GraphsComponent
  },
  {
    path:'profile',component:ProfileComponent
  }
  
  ,
  {
    path:'admin-area',component:AdminAreaComponent
  }
];
// ,
//   {
//     path:'send-email',component:SendEmailsComponent
//   }