import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { JwtService } from '../infrastructure/jwt.service';
import { InitService } from '../init/init.service';

@Component({
  selector: 'app-admin-area',
  templateUrl: './admin-area.component.html',
  styleUrls: ['./admin-area.component.scss']
})
export class AdminAreaComponent implements OnInit {
  tabType = 'ADD_STUDENT';
  isAdmin = false;
  user:any = null;
  constructor(private spinner: NgxSpinnerService ,private _initService: InitService, private _jwtService:JwtService) { }

  ngOnInit(): void {
    this.spinner.show();
    this._initService.getUser(this._jwtService.getId()).subscribe(data=>{
      if(data && data.status){
        this.spinner.hide();
        this.user = data.data;
        this.isAdmin = data.data.role === 'admin';
      }
    });
  }
  changeTab(tabName){
    this.tabType = tabName;
  }
  changeToEmailTab(){
    this.tabType = 'SEND_EMAIL';
  }
}
