import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class JwtService {
  token: string = '';
  role: string = '';
  constructor() {}
  getServiceToken() {
    return this.token;
  }
  getToken() {
    return window.sessionStorage['token'];
  }
  getId() {
    return window.sessionStorage['id'];
  }
  getServiceRole() {
    return this.role;
  }
  saveServiceToken(token: string) {
    this.token = token;
  }
  saveServiceRole(token: string) {
    this.role = token;
  }
  saveToken(token: string) {
    window.sessionStorage['token'] = token;
  }
  saveId(token: string) {
    window.sessionStorage['id'] = token;
  }
  destroyServiceToken() {
    this.token = '';
  }
  destroyServiceRole() {
    this.role = '';
  }
  destroyToken() {
    window.sessionStorage.clear();
  }
  destroyId() {
    window.sessionStorage.clear();
  }
}
