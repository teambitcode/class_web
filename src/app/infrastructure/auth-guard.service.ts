import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';

/**
 * import services
 */
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService {
  constructor(private router: Router, private _JwtService: JwtService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this._JwtService.getToken()) {
      // console.log(state.url);
      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page
    this.router.navigateByUrl('/');
    return false;
  }
}
