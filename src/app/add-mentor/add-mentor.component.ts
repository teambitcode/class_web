import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { InitService } from '../init/init.service';
import Swal from 'sweetalert2';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-add-mentor',
  templateUrl: './add-mentor.component.html',
  styleUrls: ['./add-mentor.component.scss']
})
export class AddMentorComponent implements OnInit {
  public mentorForm: FormGroup;
  currentMentor: any = null;
  currentMentorId = null;
  isEditMode = false;
  allMentors: any[] = [];
  modalRef: BsModalRef;
  constructor(private modalService: BsModalService,private spinner: NgxSpinnerService, private _initService: InitService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.getAllMentors();
    this.mentorForm = this.formBuilder.group({
      'email': [null, Validators.compose([
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required
      ])],
      'fName': [null, Validators.compose([
        Validators.required
      ])],
      'lName': [null, Validators.compose([
        Validators.required
      ])],
      'bankName': [null, Validators.compose([
        Validators.required
      ])],
      'bankAccount': [null, Validators.compose([
        Validators.required
      ])],
      'nameInBank': [null, Validators.compose([
        Validators.required
      ])],
      'contactNo': [null, Validators.compose([
        Validators.required
      ])]
    });
  }
  addMenter() {
    this.spinner.show();
    let body = new Mentor();
    body.email = this.mentorForm.get('email').value;
    body.password = this.mentorForm.get('password').value;
    body.first_name = this.mentorForm.get('fName').value;
    body.last_name = this.mentorForm.get('lName').value;
    body.bank_name = this.mentorForm.get('bankName').value;
    body.bank_account_number = this.mentorForm.get('bankAccount').value;
    body.name_in_bank_account = this.mentorForm.get('nameInBank').value;
    body.contact_number = this.mentorForm.get('contactNo').value;
    body.role = 'mentor';
    this._initService.userCreate(body).subscribe(data => {
      if (data && data.status) {
        this.getAllMentors();
        Swal.fire(
          'Added!',
          'Mentor has been added.',
          'success'
        );
        this.resetForm();
      } else {
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        });
      }
    }, err => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });

  }
  
  getAllMentors() {
    this.spinner.show();
    this._initService.getAllUsers().subscribe(data => {
      this.allMentors = data.data;
      this.spinner.hide();
    },error=>{
      this.spinner.hide();
    });
  }
  removeMentor(){
    this.spinner.show();
    this._initService.removeUser(this.currentMentorId).subscribe(data => {
      this.spinner.hide();
      this.getAllMentors()
      Swal.fire(
        'Deleted!',
        'Mentor has been deleted.',
        'success'
      );
    },error=>{
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  removeConfirm(id){
    this.currentMentorId = id;
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.removeMentor();
      }
    });
  }
  updateChagesConfirm(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, update it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.saveChanges();
      }
    });
  }
  saveChanges(){
    this.spinner.show();
    let body = new Mentor();
    body.email = this.mentorForm.get('email').value;
    body.first_name = this.mentorForm.get('fName').value;
    body.last_name = this.mentorForm.get('lName').value;
    body.bank_name = this.mentorForm.get('bankName').value;
    body.bank_account_number = this.mentorForm.get('bankAccount').value;
    body.name_in_bank_account = this.mentorForm.get('nameInBank').value;
    body.contact_number = this.mentorForm.get('contactNo').value;
    this._initService.updateUser(this.currentMentorId,body).subscribe(data => {
      if (data && data.status) {
        this.isEditMode = false;
        this.resetForm();
        this.getAllMentors();
        Swal.fire(
          'Updated!',
          'Mentor has been updated.',
          'success'
        );
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  openModal(template: TemplateRef<any>, item) {
    this.currentMentor = item;
    this.modalRef = this.modalService.show(template);
  }
  editMentor(item){
    this.mentorForm.get("password").clearValidators();
    this.mentorForm.get("password").updateValueAndValidity();
    this.isEditMode = true;
    this.currentMentorId = item._id;
    this.currentMentor = item;
    this.setFormFromGivenMentor(item);
  }
  resetForm(){
    this.mentorForm.reset();
  }
  cancel(){
    this.resetForm();
    this.isEditMode = false;
    this.currentMentorId = null;
    this.currentMentor = null;
  }
  setFormFromGivenMentor(mentorDetails){
    this.mentorForm.patchValue({email:mentorDetails.email});
    this.mentorForm.patchValue({fName:mentorDetails.first_name});
    this.mentorForm.patchValue({lName:mentorDetails.last_name});
    this.mentorForm.patchValue({bankName:mentorDetails.bank_name});
    this.mentorForm.patchValue({bankAccount:mentorDetails.bank_account_number});
    this.mentorForm.patchValue({nameInBank:mentorDetails.name_in_bank_account});
    this.mentorForm.patchValue({contactNo:mentorDetails.contact_number});
    
  }
}

export class Mentor {
  email: string;
  password:string;
  first_name: string;
  last_name: string;
  bank_name: string;
  bank_account_number: string;
  name_in_bank_account: string;
  contact_number: string;
  role: string;
}