import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { StudentClassesGroup } from '../add-student/add-student.component';
import { InitService } from '../init/init.service';
import Swal from 'sweetalert2'
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-app-student',
  templateUrl: './app-student.component.html',
  styleUrls: ['./app-student.component.scss']
})
export class AppStudentComponent implements OnInit {
  modalRef: BsModalRef;
  allStudents: any[] = [];
  allClassGroups: any[] = [];
  allClass: any[] = [];
  groupsDetails: any[] = [];
  currentClassGroupDetail: any = null;
  currentStudent: any = null;
  searchText:string = '';
  constructor(private httpx: HttpClient, private spinner: NgxSpinnerService, private _initService: InitService, private modalService: BsModalService, private router: Router) {
    this.getAllStudents();
    this.getAllClassGroups();
    this.getAllClass();
  }
  getAllClass() {
    this.spinner.show();
    this._initService.getAllClasses().subscribe(data => {
      this.allClass = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  getAllClassGroups() {
    this.spinner.show();
    this._initService.getAllGroups().subscribe(data => {
      this.allClassGroups = data.data;
      this.setGroupsDetailsArray();
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }

  getAllStudents() {
    this.spinner.show();
    this._initService.getAllStudents().subscribe(data => {
      this.allStudents = data.data;
      if (this.currentStudent) {
        this.currentStudent = this.allStudents.filter(filterStudent => {
          return filterStudent._id === this.currentStudent._id;
        })[0];
      }
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  getGroupsDetailsArrayLength(id) {
    let tempGrups: any[] = [];
    if (this.allClassGroups && this.allClassGroups.length > 0) {
      this.allClassGroups.forEach(classGroupElemet => {
        classGroupElemet.students.forEach(classGroupStudentElemet => {
          if (classGroupStudentElemet && classGroupStudentElemet.detail && classGroupStudentElemet.detail._id && classGroupStudentElemet.detail._id === id) {
            tempGrups.push(classGroupElemet);
          }
        });
      });
    }
    return tempGrups.length;
  }
  setGroupsDetailsArray() {
    let tempGrups: any[] = [];
    if (this.allClassGroups && this.allClassGroups.length > 0) {
      this.allClassGroups.forEach(classGroupElemet => {
        classGroupElemet.students.forEach(classGroupStudentElemet => {
          if (classGroupStudentElemet.detail._id === this.currentStudent._id) {
            tempGrups.push(classGroupElemet);
          }
        });
      });
    }
    this.groupsDetails = tempGrups;
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  ngOnInit(): void {
    this.getAllStudents();
  }
  displayMode(template: TemplateRef<any>, item) {
    this.currentStudent = item;
    this.setGroupsDetailsArray();
    this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
  }
  navigateWithParameters() {
    this.router.navigate(['/edit-user'], { queryParams: { user_id: "XXX001" } });
  }

  updateSlectedGroupStudentArray(groupId) {
    let student = new StudentClassesGroup();
    student.detail = this.currentStudent._id;
    student.payment_amount = 0;
    student.payment_status = "PENDING";
    let selectedGroup = this.allClassGroups.filter(filterGroupItem => {
      return filterGroupItem._id === groupId;
    });
    if (selectedGroup && selectedGroup.length > 0) {
      selectedGroup[0].students.push(student);
      return selectedGroup[0];
    } else {
      return null;
    }
  }
  removeStudentInGroup(groupId, studentData) {
    if (studentData) {
      this._initService.updateGroups(groupId, { students: studentData }).subscribe(data => {
        // this.allClassGroups = data.data;
        this.spinner.hide();
        this.getAllClassGroups();
        Swal.fire(
          'Removed!',
          'Student has been removed from group.',
          'success'
        );
      }, error => {
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        });
      });
    }

  }
  addStudentToGroup(groupId) {
    let studentDetail = this.updateSlectedGroupStudentArray(groupId);
    if (studentDetail) {
      this._initService.updateGroups(groupId, { students: studentDetail.students }).subscribe(data => {
        // this.allClassGroups = data.data;
        this.spinner.hide();
        Swal.fire(
          'Updated!',
          'Student Classes has been updated.',
          'success'
        );
        this.getAllClassGroups();
      }, error => {
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        });
      });
    }

  }
  handleEvent(event) {
    console.log(event);
    if (event.event === "ADD") {
      this.addStudentToGroup(event.data);
    } else {
      this.removeStudentInGroup(event.data.id, event.data.data);
    }

  }
}
