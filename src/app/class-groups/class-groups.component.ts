import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { InitService } from '../init/init.service';

@Component({
  selector: 'app-class-groups',
  templateUrl: './class-groups.component.html',
  styleUrls: ['./class-groups.component.scss']
})
export class ClassGroupsComponent implements OnInit {
  allClassGroups:any[] [];
  currentClassGroupDetail:any= null;
  modalRef: BsModalRef;
  totalAmount:number = 0;
  isValidClassGroupStatus = false;
  selectedClassGroupStatus = null;
  searchText = '';
  constructor(private modalService: BsModalService,private spinner: NgxSpinnerService, private _initService: InitService) { 
    this.getAllClassGroups();
  }

  ngOnInit(): void {
    this.getAllClassGroups();
  }
  openModal(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
  }
  onSelectClassGroupStatus(event){
    this.selectedClassGroupStatus = event.target.value;
    if (event.target.value && event.target.value !== "null") {
      this.isValidClassGroupStatus = true;
    } else {
      this.isValidClassGroupStatus = false;
    }
  }
  getTotalAmount(){
    let amount = 0;
    this.totalAmount;
    this.currentClassGroupDetail.students.forEach(element => {
      amount +=  element.payment_amount;
    });
    this.totalAmount = JSON.parse(JSON.stringify(amount));
  }
  displayMode(template: TemplateRef<any>,item){
    this.currentClassGroupDetail = item;
    this.getTotalAmount();
    this.modalRef = this.modalService.show(template,{class: 'modal-lg'});
  }
  getAllClassGroups() {
    this.spinner.show();
    this._initService.getAllGroups().subscribe(data => {
      this.allClassGroups = data.data;
      this.spinner.hide();
    },error=>{
      this.spinner.hide();
    });
  }

}
