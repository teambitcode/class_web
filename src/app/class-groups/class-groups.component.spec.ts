import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassGroupsComponent } from './class-groups.component';

describe('ClassGroupsComponent', () => {
  let component: ClassGroupsComponent;
  let fixture: ComponentFixture<ClassGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
