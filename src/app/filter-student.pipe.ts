import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterStudentByGroup'
})
export class filterStudentPipe implements PipeTransform {
  transform(data:any[],value: string) {
    return data.filter(item=> item.mentor._id=== value)
  }
}