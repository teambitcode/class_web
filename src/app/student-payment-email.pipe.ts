import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterPaymentEmailPipe'
})
export class FilterPaymentEmailPipe implements PipeTransform {
  transform(data:any[],value: string) {
    if(data && data.length>0){
      return data.filter((item:any)=> {
          return (item.recipt_id.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1) ||
                 (item.price.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1) ||
                 (item.class_id && item.class_id.group_id && item.class_id.group_id.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1) ||
                 (item.class.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1)||
                 (item.student_id.name && item.student_id && item.student_id.name.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1)||
                 (item.student_id.email && item.student_id && item.student_id.email.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1)
      });
    }else{
        return [];
    }

}
}