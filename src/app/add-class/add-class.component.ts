import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { InitService } from '../init/init.service';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-add-class',
  templateUrl: './add-class.component.html',
  styleUrls: ['./add-class.component.scss']
})
export class AddClassComponent implements OnInit {
  currentClassContentData: ClassContentData = new ClassContentData();
  allContentClassData: ClassContentData[] = [];
  currentTopic: string = '';
  currentSubTopic: string = '';
  currentEditIndex = null;
  currentEditIndexOfSubItem = null;
  allClass: any[] = [];
  public classForm: FormGroup;
  currentClass: any = null;
  currentClassId = null;
  currentClassContentItemId = null;
  isEditMode = false;
  isContentEditMode = false;
  currentConentNumber = null;
  isActive = null;
  constructor(private toastr: ToastrService, private spinner: NgxSpinnerService, private _initService: InitService,
    private formBuilder: FormBuilder) {
    this.getAllClass();
  }

  ngOnInit(): void {
    this.getAllClass();
    this.classForm = this.formBuilder.group({
      'classId': [null, Validators.compose([
        Validators.required
      ])],
      'img': [null, Validators.compose([
        Validators.required
      ])],
      'className': [null, Validators.compose([
        Validators.required
      ])],
      'language': [null, Validators.compose([
        Validators.required
      ])],
      'description': [null, Validators.compose([
        Validators.required
      ])],
      'duaration': [null, Validators.compose([
        Validators.required
      ])],
      'weeks': [null, Validators.compose([
        Validators.required
      ])]
    });
  }
  showSuccess(title, msg) {
    this.toastr.success(title, msg);
  }
  showError(title, msg) {
    this.toastr.error(title, msg);
  }
  editTopic(topic: any, index) {
    this.currentEditIndex = index;
    this.isContentEditMode = true;
    this.currentTopic = topic.topic;
    this.currentClassContentData.topic = topic.topic;
    this.currentClassContentData.sup_topics = topic.sup_topics;
    this.currentClassContentData = JSON.parse(JSON.stringify(this.currentClassContentData));
  }
  addSubTopic() {
    if (this.currentClassContentData.sup_topics.indexOf(this.currentSubTopic) < 0) {
      this.currentClassContentData.sup_topics.push(this.currentSubTopic);
      this.currentSubTopic = '';
      this.showSuccess('Sub Topic Added..!', null);
    } else {
      this.showError('Duplicate subtopics found', null);
    }

  }
  removeSubTopic(subTopic) {
    if (this.currentClassContentData.sup_topics) {
      let index = this.currentClassContentData.sup_topics.indexOf(subTopic);
      if (index >= 0) {
        this.currentClassContentData.sup_topics.splice(index, 1);
      }

    }
  }
  getArrayIndexByTopic(topic): number {
    let foundStatus = false;
    let indexReturn = null;
    for (let index = 0; index < this.allContentClassData.length; index++) {
      const element = this.allContentClassData[index];
      if (element.topic === topic && !foundStatus) {
        foundStatus = true;
        indexReturn = index;
      }
    }
    return indexReturn;
  }
  saveEditModeContenItem() {
    let result = this.allContentClassData.filter(filterItem => {
      return filterItem.topic.toLocaleLowerCase() === this.currentClassContentData.topic.toLocaleLowerCase();
    });
    if (result && result.length > 0) {
      if (result.length > 1) {
        this.showError('Duplicate topics found', null);
      } else {
        let returnIndex = this.getArrayIndexByTopic(this.currentClassContentData.topic);
        if (returnIndex != null) {
          this.currentClassContentData.topic = this.currentTopic;
          this.allContentClassData[returnIndex] = this.currentClassContentData;
          this.showSuccess('Content updated..!', null);
          this.currentEditIndex = null;
          this.currentEditIndexOfSubItem = null;
          this.resetCurrentForm();
          this.isContentEditMode = false;
        } else {
          this.showError('Error', null);
        }

      }

    } else {
      this.showError('Error', null);
    }

    // if(this.currentEditIndex){
    //   // this.allContentClassData[this.currentEditIndex] = this.currentClassContentData;


    // }

  }
  saveEditModeSubTopic() {
    this.currentClassContentData.sup_topics[parseInt(this.currentEditIndex) - 1] = this.currentSubTopic;
    this.currentSubTopic = '';
  }
  editModeCurrentSubTopic(subTopic, indexI) {
    if (this.currentClassContentData.sup_topics) {
      let index = this.currentClassContentData.sup_topics.indexOf(subTopic);
      if (index >= 0) {
        this.isContentEditMode = true;
        this.currentSubTopic = this.currentClassContentData.sup_topics[index];
        this.currentEditIndexOfSubItem = parseInt(indexI);
      }

    }
  }

  addFullTopicWithSubTopics() {
    let result = this.allContentClassData.filter(filterItem => {
      return filterItem.topic.toLocaleLowerCase() === this.currentTopic.toLocaleLowerCase();
    });


    if (result && result.length <= 0) {
      this.currentClassContentData.topic = this.currentTopic;
      this.allContentClassData.push(this.currentClassContentData);
      this.showSuccess('Topic Added..!', null);
      this.currentEditIndex = null;
      this.currentEditIndexOfSubItem = null;
      this.resetCurrentForm();
      this.isContentEditMode = false;
    } else {
      this.showError('Duplicate topics found', null);
    }
  }
  resetCurrentForm() {
    this.currentTopic = '';
    this.currentSubTopic = '';
    this.isContentEditMode = false;
    this.currentEditIndex = null;
    this.currentClassContentData = new ClassContentData();
  }
  addClasses() {
    this.spinner.show();
    let body = new Classes();
    body.class_id = this.classForm.get('classId').value;
    body.name = this.classForm.get('className').value;
    body.duration = this.classForm.get('duaration').value;
    body.language = this.classForm.get('language').value;
    body.number_of_weeks = this.classForm.get('weeks').value;
    body.img = this.classForm.get('img').value;
    body.description = this.classForm.get('description').value;
    body.active = true;

    body.content = [];
    this._initService.classesCreate(body).subscribe(data => {
      if (data && data.status) {
        this.getAllClass();
        this.resetForm();
        this.spinner.hide();
        Swal.fire(
          'Added!',
          'Class has been added.',
          'success'
        );
      } else {
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        });
      }
    }, err => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });

  }
  getAllClass() {
    this.spinner.show();
    this._initService.getAllClasses().subscribe(data => {
      this.allClass = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  removeClassContentItem(index) {
    this.allContentClassData.splice(index, 1);
    this.showSuccess('Content item removed', null);
  }
  removeClass() {
    this.spinner.show();
    this._initService.removeClasses(this.currentClassId).subscribe(data => {
      this.spinner.hide();
      this.getAllClass()
      Swal.fire(
        'Deleted!',
        'Class has been deleted.',
        'success'
      );
    }, error => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  removeContentItemConfirm(id) {
    this.currentClassContentItemId = id;
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.removeClassContentItem(id);
      }
    });
  }
  removeConfirm(id) {
    this.currentClassId = id;
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.removeClass();
      }
    });
  }
  updateChagesConfirm() {
    Swal.fire({
      title: 'Are you sure?',
      text: "Do you want to update?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, update it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.saveChanges();
      }
    });
  }
  updateChagesContentConfirm() {
    Swal.fire({
      title: 'Are you sure?',
      text: "Do you want to update the content?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, update it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.saveContent();
      }
    });
  }
  updateActiveStatus(status: boolean, classId) {
    let body = new Classes();
    body.active = status;
    this._initService.updateClasses(classId, body).subscribe(data => {
      if (data && data.status) {
        this.getAllClass();
        Swal.fire(
          'Updated!',
          'Status has been updated.',
          'success'
        );
        this.isContentEditMode = false;
        this.isEditMode = false;
        this.allClass = JSON.parse(JSON.stringify(this.allClass));
        this.resetForm();
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      this.allClass = JSON.parse(JSON.stringify(this.allClass));
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  saveContent() {
    let body = new Classes();
    body.content = this.allContentClassData;
    this._initService.updateClasses(this.currentClassId, body).subscribe(data => {
      if (data && data.status) {
        this.getAllClass();
        Swal.fire(
          'Updated!',
          'Content has been updated.',
          'success'
        );
        this.isContentEditMode = false;
        this.isEditMode = false;
        this.resetForm();
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  saveChanges() {
    this.spinner.show();
    let body = new Classes();
    body.class_id = this.classForm.get('classId').value;
    body.name = this.classForm.get('className').value;
    body.duration = this.classForm.get('duaration').value;
    body.language = this.classForm.get('language').value;
    body.number_of_weeks = this.classForm.get('weeks').value;
    body.img = this.classForm.get('img').value;
    body.description = this.classForm.get('description').value;
    body.active = true;
    body.content = this.allContentClassData;
    this._initService.updateClasses(this.currentClassId, body).subscribe(data => {
      if (data && data.status) {
        this.getAllClass();
        Swal.fire(
          'Updated!',
          'Class has been updated.',
          'success'
        );
        this.isEditMode = false;
        this.resetForm();
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  changeActiveStatus(event, item) {
    let status = event.target.checked ? 'Active' : 'Inactive';
    if (event.target.checked === true) {
      // Handle your code
    }
    Swal.fire({
      title: 'Are you sure?',
      text: "Do you want to update the Status to " + status + "?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, update it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.updateActiveStatus(event.target.checked, item._id);
      } else {
        this.allClass = JSON.parse(JSON.stringify(this.allClass));
      }
    });
  }
  editClass(item) {
    this.isEditMode = true;
    this.currentClassId = item._id;
    this.currentClass = item;
    this.allContentClassData = item.content ? item.content : [];
    this.setFormFromGivenClass(item);
  }
  resetForm() {
    this.classForm.reset();
  }
  cancel() {
    this.resetForm();
    this.isEditMode = false;
    this.currentClassId = null;
    this.currentClass = null;
  }
  setFormFromGivenClass(classDetails) {
    this.classForm.patchValue({ classId: classDetails.class_id });
    this.classForm.patchValue({ className: classDetails.name });
    this.classForm.patchValue({ language: classDetails.language });
    this.classForm.patchValue({ duaration: classDetails.duration });
    this.classForm.patchValue({ weeks: classDetails.number_of_weeks });
    this.classForm.patchValue({ img: classDetails.img });
    this.classForm.patchValue({ description: classDetails.description });
    // this.classForm.patchValue({ active: classDetails.description });

  }
}

export class Classes {
  class_id: string;
  name: string;
  language: string;
  duration: number;
  number_of_weeks: number;
  content: ClassContentData[] = [];
  img: string;
  description: string;
  active: boolean;
}

export class ClassContentData {
  topic: string;
  sup_topics: any[] = [];
}
