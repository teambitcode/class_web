import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterClassGroupDetails'
})
export class FilterClassGroupByDetails implements PipeTransform {
  transform(data:any[],value: string) {
      if(data && data.length>0){
        return data.filter((item:any)=> {
            return (item.class_id._id.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1) ||
                   (item.group_id.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1) ||
                   (item.name.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1) ||
                   (item.mentor.first_name.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1) ||
                   (item.mentor.last_name.toLowerCase().toString().indexOf(value.toString().toLowerCase()) !== -1) ||
                   (item.name.toString().toLowerCase().indexOf(value.toString()) !== -1)
            
        });
      }else{
          return [];
      }
    
  }
}