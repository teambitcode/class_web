import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2'
import { JwtService } from '../infrastructure/jwt.service';
import { InitService } from '../init/init.service';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  isAdmin = false;
  user:any = null;
  constructor(private router: Router,private spinner: NgxSpinnerService ,private _initService: InitService, private _jwtService:JwtService) { }

  ngOnInit(): void {
    this.spinner.show();
    this._initService.getUser(this._jwtService.getId()).subscribe(data=>{
      if(data && data.status){
        this.spinner.hide();
        this.user = data.data;
        this.isAdmin = data.data.role === 'admin';
      }
    });
  }
  logOut(){
    Swal.fire({
      title: 'Are you sure?',
      text: "Do yo need to log out?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.isConfirmed) {
        this._jwtService.destroyId();
        this.router.navigateByUrl('/login');
        // Swal.fire(
        //   'Successfully logout',
        //   'success'
        // )
      }
    });
  }

}
