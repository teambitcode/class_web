import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countInArray'
})
export class CountInArrayPipe implements PipeTransform {

  transform(value: any[], checkingField: string, checkingValue:any): number {
    let count = 0;
    if(value && value.length>0){
      value.forEach(element=>{
          if(element[checkingField] && element[checkingField] === checkingValue){
            count += 1;
          }
      });
    }

    return count;
  }

}
