import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterGroupsByClass'
})
export class filterPipe implements PipeTransform {
  transform(data:any[],value: string) {
    return data.filter(item=> item.class_id._id=== value)
  }
}