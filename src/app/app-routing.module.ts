import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { aboutChildRoutes, AboutComponent } from './about/about.component';
import { AppEditUserComponent } from './app-edit-user/app-edit-user.component';
import { AppLandingComponent, landingChildRoutes } from './app-landing/app-landing.component';
import { AppLoginComponent } from './app-login/app-login.component';
import { NewStudentsComponent } from './new-students/new-students.component';

const routes: Routes = [
  {
    path:'',component: AppLoginComponent
  },
  {
    path:'about',component: AboutComponent,
    children:aboutChildRoutes
  },
  {
    path:'base', component:AppLandingComponent,
    children: landingChildRoutes
  },
  {
    path:'login',component: AppLoginComponent
  },
  {
    path:'edit-user',component:AppEditUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
