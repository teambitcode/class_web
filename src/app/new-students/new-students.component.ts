import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { InitService } from '../init/init.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-new-students',
  templateUrl: './new-students.component.html',
  styleUrls: ['./new-students.component.scss']
})
export class NewStudentsComponent implements OnInit {
  public classNewForm: FormGroup;
  allClass: any[] = [];
  jsValue:number = 0;
  javaValue:number = 0;
  submitted:false;
  constructor(private _initService: InitService, private spinner: NgxSpinnerService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.getAllClass();
    this.classNewForm = this.formBuilder.group({
      'class_id': [null, Validators.compose([
        Validators.required
      ])],
      'contact_number': [null, Validators.compose([
        Validators.required, Validators.minLength(10), Validators.maxLength(10)
      ])],
      'email': [null, Validators.compose([
        Validators.required, Validators.email
      ])]
      ,
      'name': [null, Validators.compose([
        Validators.required
      ])]
    });
    this.classNewForm.patchValue({ 'class_id': 'default' });
  }
  getAllClass() {
    this.spinner.show();
    this._initService.getAllClasses().subscribe(data => {
      this.allClass = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  radioChangeJava(event) {
    console.log(event);
    this.classNewForm.patchValue({ 'java_knowledge': 1 });
  }

  submitConfirm() {
    // this.classNewForm.
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, Proceed!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.registerStudent();
      }
    });
  }
  registerStudent(){
      this.spinner.show();
      let body = new NewStudent();
      body.class_id = this.classNewForm.get('class_id').value;
      body.name = this.classNewForm.get('name').value;
      body.email = this.classNewForm.get('email').value;
      body.contact_number = this.classNewForm.get('contact_number').value;
      body.java_knowledge = this.jsValue;
      body.javascript_knowledge = this.javaValue;
      body.is_confirmed = 'N';

      this._initService.newStudentCreate(body).subscribe(data => {
        if (data && data.status) {
          this.resetForm();
          this.spinner.hide();
          Swal.fire(
            'Registerd Successfully',
            'Please check your email',
            'success'
          );
        } else {
          this.spinner.hide();
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!'
          });
        }
      }, err => {
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        });
      });
  
    
  }
  resetForm() {
    this.classNewForm.reset();
    this.classNewForm.patchValue({ 'class_id': 'default' });
    this.jsValue = 0;
    this.javaValue = 0;
  }
  getJs(event, value) {
    console.log(event.target.checked);
    console.log(value);
    this.jsValue = value;
  
  }
  getJava(event, value) {
    console.log(event.target.checked);
    console.log(value);
    this.javaValue = value;
  }
}

export class NewStudent {
  class_id: string;
  name: string;
  contact_number: string;
  email: string;
  javascript_knowledge: number;
  java_knowledge: number;
  is_confirmed: string;
  confirm_student: string;
}