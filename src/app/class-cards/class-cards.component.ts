import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { InitService } from '../init/init.service';

@Component({
  selector: 'app-class-cards',
  templateUrl: './class-cards.component.html',
  styleUrls: ['./class-cards.component.scss']
})
export class ClassCardsComponent implements OnInit {
  allClass: any[] = [];
  constructor(private router:Router, private spinner: NgxSpinnerService, private _initService: InitService) { }

  ngOnInit(): void {
    this.getAllClass();
  }

  getAllClass() {
    this.spinner.show();
    this._initService.getAllClasses().subscribe(data => {
      this.allClass = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  getTags(tagString:string){
    let tagArray = tagString.split(',');
    return tagArray && tagArray.length>0 ? tagArray : [];
  }
  gotoContent(item){
    if(item && item.active){
      let navigationExtras: NavigationExtras = {
        queryParams: {
           "class_id": item._id
        }
      };
     this.router.navigate(['/about/content'], navigationExtras)
    }

  }
}
