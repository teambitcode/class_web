import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.scss']
})
export class UserModalComponent implements OnInit {
@Input() data:any;
  constructor() { }

  ngOnInit(): void {
  }

}
