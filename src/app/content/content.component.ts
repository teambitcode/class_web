import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { fadeInLeftOnEnterAnimation, 
  fadeOutLeftOnLeaveAnimation,
  fadeInRightOnEnterAnimation, 
  fadeOutRightOnLeaveAnimation,
rubberBandAnimation } from 'angular-animations';
import { param } from 'jquery';
import { NgxSpinnerService } from 'ngx-spinner';
import { InitService } from '../init/init.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  animations: [
    fadeInLeftOnEnterAnimation(),
    fadeOutLeftOnLeaveAnimation(),
    fadeInRightOnEnterAnimation(),
    fadeOutRightOnLeaveAnimation(),
    rubberBandAnimation()
  ]
})
export class ContentComponent implements OnInit {
currentClass:any = null;
classId= null;
classTopic= null;
contentArray;
public navbarCollapsed:any={};
  constructor(private router:Router,private route: ActivatedRoute,private spinner: NgxSpinnerService, private _initService: InitService) { }

  ngOnInit(): void {
    this.route.queryParams
    .subscribe(params => {
      console.log(params); // { order: "popular" }
      if(params && params.class_id){
        this.classId = params.class_id;
        this.getAllClassContent(params.class_id);
      }else{
        this.router.navigateByUrl('about/classes');
      }
    }
  );
}
getAllClassContent(id) {
  this.spinner.show();
  this._initService.getClasses(id).subscribe(data => {
    if(data && data.data && data.data.content){
      this.currentClass = data.data;
      this.addElemnt();
      this.spinner.hide();
    }

  }, error => {
    this.spinner.hide();
  });
}
addElemnt(){
  this.currentClass.content.forEach(element => {
    element['expand'] = true;
  });
}

handleCollaps(item){
  item.expand = !item.expand;
}
getTags(tagString:string){
  let tagArray = tagString.split(',');
  return tagArray && tagArray.length>0 ? tagArray : [];
}
  }


