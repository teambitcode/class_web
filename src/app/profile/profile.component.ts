import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { InitService } from '../init/init.service';
import Swal from 'sweetalert2';
import { Mentor } from '../add-mentor/add-mentor.component';
import { JwtService } from '../infrastructure/jwt.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public mentorForm: FormGroup;
  public mentorFormPassword: FormGroup;
  currentMentor: any = null;
  currentMentorId = null;
  isEditMode = false;
  allMentors: any[] = [];
  constructor(private spinner: NgxSpinnerService, private _initService: InitService,
    private formBuilder: FormBuilder, private _jwtService: JwtService) { }

  ngOnInit(): void {
    this.getCurrentUserDetails();
    // this.getAllMentors();
    this.mentorFormPassword = this.formBuilder.group({
      'password': [null, Validators.compose([
        Validators.required
      ])],
      'conf_password': [null, Validators.compose([
        Validators.required
      ])]
    });

    this.mentorForm = this.formBuilder.group({
      'email': [null, Validators.compose([
        Validators.required
      ])],
      'fName': [null, Validators.compose([
        Validators.required
      ])],
      'lName': [null, Validators.compose([
        Validators.required
      ])],
      'bankName': [null, Validators.compose([
        Validators.required
      ])],
      'bankAccount': [null, Validators.compose([
        Validators.required
      ])],
      'nameInBank': [null, Validators.compose([
        Validators.required
      ])],
      'contactNo': [null, Validators.compose([
        Validators.required
      ])]
    });
  }
  setFormValues(){
    this.spinner.show();
    this.mentorForm.patchValue({email:this.currentMentor.email});
    this.mentorForm.patchValue({fName:this.currentMentor.first_name});
    this.mentorForm.patchValue({lName:this.currentMentor.last_name});
    this.mentorForm.patchValue({bankName:this.currentMentor.bank_name});
    this.mentorForm.patchValue({bankAccount:this.currentMentor.bank_account_number});
    this.mentorForm.patchValue({nameInBank:this.currentMentor.name_in_bank_account});
    this.mentorForm.patchValue({contactNo:this.currentMentor.contact_number});
    this.spinner.hide();
  }
  getCurrentUserDetails() {
    this.spinner.show();
    this._initService.getUser(this._jwtService.getId()).subscribe(userData => {
      if (userData && userData.status) {
        this.currentMentor = userData.data;
        this.setFormValues();
      }
    });
  }
  updateChagesConfirm() {
    Swal.fire({
      title: 'Are you sure?',
      text: "Do you need to update your account?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, update it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.saveChanges();
      }
    });
  }
  validatePassword(){
    let newPassword = this.mentorFormPassword.get('password').value;
    let newConfPassword = this.mentorFormPassword.get('conf_password').value;
    if(newPassword !== newConfPassword){
      Swal.fire({
        icon: 'error',
        title: 'Password not matched!',
        text: 'New Password and Confirm Password not matched!'
      });
      return false;
    }else{
      return true;
    }
  }
  saveChanges() {
    this.spinner.show();
    let body = new Mentor();
    body.email = this.mentorForm.get('email').value;
    body.first_name = this.mentorForm.get('fName').value;
    body.last_name = this.mentorForm.get('lName').value;
    body.bank_name = this.mentorForm.get('bankName').value;
    body.bank_account_number = this.mentorForm.get('bankAccount').value;
    body.name_in_bank_account = this.mentorForm.get('nameInBank').value;
    body.contact_number = this.mentorForm.get('contactNo').value;
    this._initService.updateUser(this._jwtService.getId(), body).subscribe(data => {
      if (data && data.status) {
        this.isEditMode = false;
        this.spinner.hide();
        Swal.fire(
          'Updated!',
          'Mentor has been updated.',
          'success'
        );
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  updatePasswordConfirm(){
    if(this.validatePassword()){
      this.spinner.show();
      let body:any = {};
      body.password = this.mentorFormPassword.get('password').value;
      this._initService.updateUserPassword(this._jwtService.getId(), body).subscribe(data => {
        if (data && data.status) {
          this.isEditMode = false;
          this.spinner.hide();
          Swal.fire(
            'Updated!',
            'Password updated.',
            'success'
          );
        } else {
          this.spinner.hide();
        }
      }, err => {
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        });
      });
    }
  }
}
