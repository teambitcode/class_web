import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupDetailModalComponent } from './group-detail-modal.component';

describe('GroupDetailModalComponent', () => {
  let component: GroupDetailModalComponent;
  let fixture: ComponentFixture<GroupDetailModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupDetailModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupDetailModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
