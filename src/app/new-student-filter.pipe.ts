import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterNewStudentByAll'
})
export class filterNewStudentByAllPipe implements PipeTransform {
  transform(data:any[],value: string) {
    if(data && data.length>0){
      return data.filter((item:any)=> {
          return (item.email.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1) ||
                 (item.name.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1) ||
                 (item.class_id.name.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1) ||
                 (item.contact_number.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1)
      });
    }else{
        return [];
    }

}
}