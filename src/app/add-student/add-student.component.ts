import { Component, EventEmitter, OnInit, Output, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { InitService } from '../init/init.service';
import Swal from 'sweetalert2';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DataService } from '../data.service';
@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.scss']
})
export class AddStudentComponent implements OnInit {
  allStudents: any[] = [];
  allClass: any[] = [];
  allClassGroups: any[] = [];
  currentStudent: any = null;
  currentStudentId = null;
  isEditMode = false;
  selectedClassId:string = '';
  public studentForm: FormGroup;
  public studentAddGroupForm: FormGroup;
  modalRef: BsModalRef;
  isValidCladd = false;
  isValidGroup = false;
  searchText:string= '';
  @Output() emailEvent = new EventEmitter();
  constructor(private modalService: BsModalService,private spinner: NgxSpinnerService, private _initService: InitService,
    private formBuilder: FormBuilder,private dataService: DataService) {
    this.getAllStudents();
    this.getAllClassGroups();
    this.getAllClass();
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  ngOnInit(): void {
    this.getAllStudents();
    this.studentForm = this.formBuilder.group({
      'email': [null, Validators.compose([
        Validators.required
      ])],
      'nicNo': [null, Validators.compose([
        Validators.required
      ])],
      'name': [null, Validators.compose([
        Validators.required
      ])],
      'gender': [null, Validators.compose([
        Validators.required
      ])],
      'contactNo': [null, Validators.compose([
        Validators.required
      ])]
    });
    this.studentAddGroupForm = this.formBuilder.group({
      'classId': [null, Validators.compose([
        Validators.required
      ])],
      'groupId': [null, Validators.compose([
        Validators.required
      ])]
    });
  }
  onSelectClassId(event) {
    this.selectedClassId = event.target.value;
    if (event.target.value && event.target.value !== "null") {
      this.isValidCladd = true;
    } else {
      this.isValidCladd = false;
    }
  }
  onSelectGroupId(event) {
    if (event.target.value && event.target.value !== "null") {
      this.isValidGroup = true;
    } else {
      this.isValidGroup = false;
    }
  }
  getAllClassGroups() {
    this.spinner.show();
    this._initService.getAllGroups().subscribe(data => {
      this.allClassGroups = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  getAllClass() {
    this.spinner.show();
    this._initService.getAllClasses().subscribe(data => {
      this.allClass = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  addStudent() {
    this.spinner.show();
    let body = new Student();
    body.email = this.studentForm.get('email').value;
    body.name = this.studentForm.get('name').value;
    body.nic_number = this.studentForm.get('nicNo').value;
    body.conact_number = this.studentForm.get('contactNo').value;
    body.gender = this.studentForm.get('gender').value;
    this._initService.studentCreate(body).subscribe(data => {
      if (data && data.status) {
        this.getAllStudents();
        Swal.fire(
          'Added!',
          'Student has been added.',
          'success'
        );
        this.resetForm();
      } else {
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!'
        });
      }
    }, err => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });

  }

  getAllStudents() {
    this.spinner.show();
    this._initService.getAllStudents().subscribe(data => {
      this.allStudents = data.data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  removeStudent() {
    this.spinner.show();
    this._initService.removeStudent(this.currentStudentId).subscribe(data => {
      this.spinner.hide();
      this.getAllStudents()
      Swal.fire(
        'Deleted!',
        'Student has been deleted.',
        'success'
      );
    }, error => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  removeConfirm(id) {
    this.currentStudentId = id;
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.removeStudent();
      }
    });
  }
  updateChagesConfirm() {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes, update it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.saveChanges();
      }
    });
  }
  saveChanges() {
    this.spinner.show();
    let body = new Student();
    body.email = this.studentForm.get('email').value;
    body.name = this.studentForm.get('name').value;
    body.nic_number = this.studentForm.get('nicNo').value;
    body.conact_number = this.studentForm.get('contactNo').value;
    body.gender = this.studentForm.get('gender').value;
    this._initService.updateStudents(this.currentStudentId, body).subscribe(data => {
      if (data && data.status) {
        this.isEditMode = false;
        this.resetForm();
        this.getAllStudents();
        Swal.fire(
          'Updated!',
          'Student has been updated.',
          'success'
        );
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }

  editStudent(item) {
    this.isEditMode = true;
    this.currentStudentId = item._id;
    this.currentStudent = item;
    this.setFormFromGivenStudent(item);
  }
  resetForm() {
    this.studentForm.reset();
  }
  sendEmail(item){
    this.dataService.student = item;
    this.emailEvent.emit(true);
  }
  cancel() {
    this.resetForm();
    this.isEditMode = false;
    this.currentStudent = null;
    this.currentStudentId = null;
  }
  setFormFromGivenStudent(studentDetails) {
    this.studentForm.patchValue({ email: studentDetails.email });
    this.studentForm.patchValue({ nicNo: studentDetails.nic_number });
    this.studentForm.patchValue({ name: studentDetails.name });
    this.studentForm.patchValue({ gender: studentDetails.gender });
    this.studentForm.patchValue({ contactNo: studentDetails.conact_number });

  }
  addStudentToGroupConfirm() {

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#21d44a',
      cancelButtonColor: 'rgb(251 29 29)',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.isConfirmed) {
        this.addStudentToGroup();
      }
    });

  }
  filterClasGroupByGroupId(group_id) {
    let classGrops = this.allClassGroups.filter(filterItem => {
      return filterItem._id === group_id
    });
    return classGrops.length > 0 ? JSON.parse(JSON.stringify(classGrops[0])) : null;
  }
  addStudentToGroup() {
    this.spinner.show();
    let body: any = {};
    let group_id = this.studentAddGroupForm.get('groupId').value;
    let classGroup = new StudentClassesGroup();
    classGroup.detail = this.currentStudentId;
    classGroup.payment_amount = 0;
    classGroup.payment_status = "PENDING";

    let selectedGroup = this.filterClasGroupByGroupId(group_id);
    selectedGroup.students.push(classGroup);
    body.students = selectedGroup.students;
    this._initService.updateGroups(group_id, body).subscribe(data => {
      if (data && data.status) {
        this.isEditMode = false;
        this.getAllStudents();
        this.getAllClassGroups();
        Swal.fire(
          'Updated!',
          'Student Classes has been updated.',
          'success'
        );
      } else {
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });
    });
  }
  getJoinedClasses(studentId) {
    let classCount = 0;
    this.allClassGroups.forEach(groupItem => {

      let studentGroup = (groupItem && groupItem.students) ? groupItem.students.filter(filterGroupStudentItem => {
        return filterGroupStudentItem && filterGroupStudentItem.detail && filterGroupStudentItem.detail._id === studentId
      }) : [];
      if (studentGroup.length > 0) {
        classCount += 1;
      }
    });
    return classCount;
  }
}

export class Student {
  email: string;
  name: string;
  nic_number: string;
  gender: string;
  conact_number: string;
}

export class StudentClasses {
  class_id: string;
  group_id: string;
}

export class StudentClassesGroup {
  detail: string;
  payment_status: string;
  payment_amount: number;
}